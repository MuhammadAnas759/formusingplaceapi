package com.example.android.pdfviewer;

public class FIleObject {
   private String id,origin,destination,area,weight,noOfTrucks,time,date,comment;

    public FIleObject(String id,String origin, String destination, String area, String weight, String noOfTrucks, String time, String date, String comment) {
        this.id=id;
        this.origin = origin;
        this.destination = destination;
        this.area = area;
        this.weight = weight;
        this.noOfTrucks = noOfTrucks;
        this.time = time;
        this.date = date;
        this.comment = comment;
    }

    public FIleObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getNoOfTrucks() {
        return noOfTrucks;
    }

    public void setNoOfTrucks(String noOfTrucks) {
        this.noOfTrucks = noOfTrucks;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
