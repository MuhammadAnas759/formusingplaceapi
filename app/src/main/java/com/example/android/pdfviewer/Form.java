package com.example.android.pdfviewer;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Form extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemSelectedListener {
private static final String TAG="Form_Activity";
private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));

    private AutoCompleteTextView origin,destination,marea;
    Button time,date,post;
    EditText comment;
    String mTime=null,mDate=null;
    Spinner mtruck,mweight;
    //string for file obj
String weightname,noTrucksname;
    //variables
//    private boolean mLocationPermissionGranted=false;
//    private GoogleMap mMap;
//    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private GoogleApiClient mGoogleApiClient;
    //private PlaceInfo mPlace;
    //private Marker mMarker;
    String[] noOfTrucks={"no.of trucks","1","2","3","4","5"};
    String[] weight={"Select Weight","Full","Partial"};
    //FireBase
    DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: Before Layout");
        setContentView(R.layout.activity_form);
        Log.d(TAG, "onCreate: After Layout");
   // firebase
mDatabase= FirebaseDatabase.getInstance().getReference("fileObject");
        marea=(AutoCompleteTextView)findViewById(R.id.area);
        mtruck=(Spinner)findViewById(R.id.truck);
        mweight=(Spinner)findViewById(R.id.weight);
        origin=(AutoCompleteTextView)findViewById(R.id.origin);
        destination=(AutoCompleteTextView)findViewById(R.id.desination);
        time=(Button)findViewById(R.id.time);
        date=(Button)findViewById(R.id.date);
        post=(Button)findViewById(R.id.post);
        comment=(EditText)findViewById(R.id.comment);
        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat TimeFormat=new SimpleDateFormat("HH:mm:ss");
        mTime=TimeFormat.format(calendar.getTime());
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        mDate=simpleDateFormat.format(calendar.getTime());

        //===========Click Listner============
        Log.d(TAG, "onCreate: before prompt");
        mtruck.setSelection(0);

        //mtruck.setPrompt("No. Of Trucks");
//        ArrayList<String> initValues=new ArrayList<String>(){};
//        initValues.add("No.Of_Trucks");
//        initValues.add("Weight");

        mtruck.setOnItemSelectedListener(this);

        ArrayAdapter truckAdapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,noOfTrucks);
        truckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mtruck.setAdapter(truckAdapter);
        // for Weight
        mweight.setSelection(0);
        mweight.setOnItemSelectedListener(this);

        ArrayAdapter weightAdapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,weight);
        weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mweight.setAdapter(weightAdapter);


        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time.setText(mTime);

            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date.setText(mDate);
            }
        });
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Form.this, "Post Successfully", Toast.LENGTH_LONG).show();
            }
        });
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        destination.setOnItemClickListener(mAutocompleteClickListener);
        origin.setOnItemClickListener(mAutocompleteClickListener);
        AutocompleteFilter typeFilter=new AutocompleteFilter.Builder().setCountry("PK").build();
        mPlaceAutocompleteAdapter= new PlaceAutocompleteAdapter(this, mGoogleApiClient,LAT_LNG_BOUNDS,typeFilter);

        destination.setAdapter(mPlaceAutocompleteAdapter);
        origin.setAdapter(mPlaceAutocompleteAdapter);
        destination.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                Log.d(TAG, "onEditorAction(Destination): before if");
                if(i == EditorInfo.IME_ACTION_SEARCH
                        || i == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER){
                    Log.d(TAG, "onEditorAction: inside init");

                    //execute our method for searching
                    geoLocate(destination);
                }
                return false;
            }
        });
        origin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                Log.d(TAG, "onEditorAction(Origin): before if");
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER){
                    Log.d(TAG, "onEditorAction: inside init");

                    //execute our method for searching
                    geoLocate(origin);
                }

                return false;
            }
        });
    post.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: post clicked");
            addFile();
        }
    });
    }
    private void addFile() {
        Log.d(TAG, "addFile: addfile");
        
        //getting the values to save
        String originname = origin.getText().toString().trim();
        String destname = destination.getText().toString().trim();
        String comments=comment.getText().toString().trim();
        String area=marea.getText().toString().trim();
//boolean chk1=false;
        //checking if the value is provided
        if (!TextUtils.isEmpty(originname)) {

            //getting a unique id using push().getKey() method
            //it will create a unique id and we will use it as the Primary Key for our Artist
            String id = mDatabase.push().getKey();

            //creating an File Object
            FIleObject file=new FIleObject(id,originname,destname,area,weightname,noTrucksname,mTime,mDate,comments);

            //Saving the Artist
            mDatabase.child(id).setValue(file);


            Log.d(TAG, "addFile: before set text");
            //setting edittext to blank again
            origin.setText("");
            destination.setText("");
            marea.setText("");
            mtruck.setSelection(0);
            mweight.setSelection(0);
            time.setText("GET TIME");
            date.setText("GET DATE");
            comment.setText("");

            //displaying a success toast
            Toast.makeText(this, "File added", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Log.d(TAG, "addFile: Please enter a name");
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }
    }

    private void geoLocate(AutoCompleteTextView autoCompleteTextView){
        Log.d(TAG, "geoLocate: geolocating");

        String searchString = autoCompleteTextView.getText().toString();

        Geocoder geocoder = new Geocoder(Form.this);
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(searchString, 1);
        }catch (IOException e){
            Log.e(TAG, "geoLocate: IOException: " + e.getMessage() );
        }

        if(list.size() > 0){
            Address address = list.get(0);

            Log.d(TAG, "geoLocate: found a location: " + address.toString());
//            moveCamera(new LatLng(address.getLatitude(),address.getLongitude()),DEFAULT_ZOOM,address.getAddressLine(0));
            //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();

        }
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            final AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(i);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if(!places.getStatus().isSuccess()){
                Log.d(TAG, "onResult: Place query did not complete successfully: " + places.getStatus().toString());
                places.release();
                return;
            }
            final Place place = places.get(0);

            try{
                Log.d(TAG, "onResult: locale: "+place.getLocale());

                Log.d(TAG, "onResult:addr: "+place.getAddress());
                Log.d(TAG, "onResult:name:  "+place.getName());
                Log.d(TAG, "onResult: attr: "+place.getAttributions());
//                mPlace = new PlaceInfo();
//                mPlace.setName(place.getName().toString());
//                Log.d(TAG, "onResult: name: " + place.getName());
//                mPlace.setAddress(place.getAddress().toString());
//                Log.d(TAG, "onResult: address: " + place.getAddress());
////                mPlace.setAttributions(place.getAttributions().toString());
////                Log.d(TAG, "onResult: attributions: " + place.getAttributions());
//                mPlace.setId(place.getId());
//                Log.d(TAG, "onResult: id:" + place.getId());
//                mPlace.setLatlng(place.getLatLng());
//                Log.d(TAG, "onResult: latlng: " + place.getLatLng());
//                mPlace.setRating(place.getRating());
//                Log.d(TAG, "onResult: rating: " + place.getRating());
//                mPlace.setPhoneNumber(place.getPhoneNumber().toString());
//                Log.d(TAG, "onResult: phone number: " + place.getPhoneNumber());
//                mPlace.setWebsiteUri(place.getWebsiteUri());
//                Log.d(TAG, "onResult: website uri: " + place.getWebsiteUri());

            }catch (NullPointerException e){
                Log.e(TAG, "onResult: NullPointerException: " + e.getMessage() );
            }


            places.release();
        }
    };

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
       Log.d(TAG, "onItemSelected: ");
       switch (adapterView.getId()){
           case R.id.weight:
           mweight.setPrompt(weight[i]);
           weightname=weight[i];
               break;
           case R.id.truck:
               mtruck.setPrompt(noOfTrucks[i]);
               noTrucksname=noOfTrucks[i];
               break;
            
           default:
               Log.d(TAG, "onItemSelected: default");

       }
//        Log.d(TAG, "onItemSelected: "+adapterView.getAdapter().getItem(i).toString());
//        Toast.makeText(Form.this, ""+noOfTrucks[i], Toast.LENGTH_SHORT).show();
//        
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
