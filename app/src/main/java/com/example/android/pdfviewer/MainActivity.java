package com.example.android.pdfviewer;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shockwave.pdfium.PdfiumCore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity{
    private static final String TAG="MainActivity";
  private Button select,open;
  private ImageView pdfimage;
  static ImageView imgTakenPic;
    EditText comment;
    TextView date, time,file_name;
   private int currentPage=0;
    private static final int CAM_REQUEST = 1313;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int RESULT_LOAD_FILE = 2;
    private static final int REQUEST_CODE=43;

    Button insert;
    private byte[] image;
    String DateVar;
    String TimeVar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: before xml");
        setContentView(R.layout.activity_main);
        select=(Button)findViewById(R.id.pdffind);
        open=(Button)findViewById(R.id.formopen); 
imgTakenPic=(ImageView)findViewById(R.id.imageView);

open.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick: Button Clicked...");
        Intent intent=new Intent(MainActivity.this,Form.class);
        startActivity(intent);
    }
});
select.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View view) {
        Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("application/pdf");
        Log.d(TAG, "onClick: inside Onclick");
        startActivityForResult(i, RESULT_LOAD_FILE);
    }
});
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: Request Code"+requestCode);

        if(requestCode==RESULT_LOAD_FILE)
        {
            if (data != null && data.getData() != null && resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "onActivityResult: if data!=null "+ resultCode+" == "+Activity.RESULT_OK);
                Uri uri=data.getData();
                String t=getMimeType(uri);
                if(t.equals("application/pdf")) {
                    Log.d(TAG, "onActivityResult: it is PDF t = "+t);
                    generateImageFromPdf(uri);
                }
                else
                if(t.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")||t.equals("text/plain")||t.equals("application/msword"))
                {
                    openFile(uri,t);
                }

            }
        }

    }
    public String getMimeType(Uri uri) {
        Log.d(TAG, "getMimeType: "+uri.getPath());
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    void openFile(final Uri myuri,final String t)
    {
        Log.d(TAG, "openFile: "+myuri.getPath()+""+t.toString());
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.dialogbox);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // set the custom dialog components - text, image and button
        time = (TextView) dialog.findViewById(R.id.time);
        comment = (EditText) dialog.findViewById(R.id.comment);
        date = (TextView) dialog.findViewById(R.id.date);
  //      file_name=(TextView)dialog.findViewById(R.id.file_name);
        time.setText(TimeVar);
        date.setText(DateVar);
        File file= new File(myuri.getPath());
        file.getName();
        file_name.setText(file.getName());

        file_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(myuri,t)  ;
                startActivity(intent);
//                try {
//                    startActivity(intent);
//                } catch (ActivityNotFoundException e) {
//                    int time = 10;
//                    for (int i = 0; i < time; i++) {
//                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
//                    }
//                }

            }
        });
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    void generateImageFromPdf(final Uri pdfUri) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(MainActivity.this);
        try {
            Calendar calendar=Calendar.getInstance();
            SimpleDateFormat TimeFormat=new SimpleDateFormat("HH:mm:ss");
            TimeVar=TimeFormat.format(calendar.getTime());
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
            DateVar=simpleDateFormat.format(calendar.getTime());

            pdfimage=(ImageView)findViewById(R.id.pdfview);
            //http://www.programcreek.com/java-api-examples/index.php?api=android.os.ParcelFileDescriptor
            ParcelFileDescriptor fd = getContentResolver().openFileDescriptor(pdfUri, "r");
            com.shockwave.pdfium.PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);

            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

//            Matrix m = pdfimage.getImageMatrix()
// ;
             Log.d(TAG, "onActivityResult: image height" + pdfimage.getHeight() + " width" + pdfimage.getWidth());
            Log.d(TAG, "onActivityResult: bitmap height" + height + " width" + width);
              //Rect rect = new Rect(0, 0, pdfimage.getWidth(), pdfimage.getHeight());

            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0,width ,height);
            pdfimage.setImageBitmap(bmp);
            pdfimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: clickListner of image button");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(pdfUri, "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    Intent ni = Intent.createChooser(intent, "Open File");

                    try {
                        startActivity(ni);
                    } catch (ActivityNotFoundException e) {
                        //if user doesn't have pdf reader instructing to download a pdf reader
                    }
                }
            });

            final Dialog dialog = new Dialog(MainActivity.this);
            Log.d(TAG, "generateImageFromPdf: before dailog");
            dialog.setContentView(R.layout.dialogbox);
            Log.d(TAG, "generateImageFromPdf: after dailog");
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.background_light);
            // set the custom dialog components - text, image and button
            time = (TextView) dialog.findViewById(R.id.time);
            comment = (EditText) dialog.findViewById(R.id.comment);
            date = (TextView) dialog.findViewById(R.id.date);
            ImageView image = (ImageView) dialog.findViewById(R.id.imageView);
            Log.d(TAG, "generateImageFromPdf: before if");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                image.setClipToOutline(true);
            }
            image.setImageBitmap(bmp);

            time.setText(TimeVar);
            date.setText(DateVar);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: clickListner of open button");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(pdfUri, "application/pdf");

                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //if user doesn't have pdf reader instructing to download a pdf reader
                    }

                }
            });

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);

            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            Log.d(TAG, "generateImageFromPdf: dialog show");
            dialog.show();
            pdfiumCore.closeDocument(pdfDocument); // important!
        } catch(Exception e) {
            Log.d(TAG, "generateImageFromPdf:  exception occurred");
            //todo with exception
        }
    }


//    public void startSearch(){
//        Log.d(TAG, "startSearch: in start Search");
//        Intent intent=new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        intent.setType("application/pdf");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        startActivityForResult(intent,REQUEST_CODE);
//}

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==REQUEST_CODE && resultCode== Activity.RESULT_OK){
//
//        if(data!=null){
//            try {
//                Log.d(TAG, "onActivityResult: current Page" + currentPage);
////              Toast.makeText(MainActivity.this, "uri" + uri, Toast.LENGTH_SHORT).show();
////                Toast.makeText(MainActivity.this, "path" + uri.getPath(), Toast.LENGTH_SHORT).show();
//                File file = new File(data.getDataString());
//                Log.d(TAG, "onActivityResult: "+file);
//                ParcelFileDescriptor fileDescriptor = null;
//                fileDescriptor = ParcelFileDescriptor.open(
//                        file, ParcelFileDescriptor.MODE_READ_ONLY);
//
//                //min. API Level 21
//                PdfRenderer pdfRenderer = null;
//                pdfRenderer = new PdfRenderer(fileDescriptor);
//
//                final int pageCount = pdfRenderer.getPageCount();
//                Toast.makeText(this,
//                        "pageCount = " + pageCount,
//                        Toast.LENGTH_LONG).show();
//
//                //Display page 0
//                PdfRenderer.Page rendererPage = pdfRenderer.openPage(0);
//                int rendererPageWidth = rendererPage.getWidth();
//                int rendererPageHeight = rendererPage.getHeight();
//                Bitmap bitmap = Bitmap.createBitmap(
//                        rendererPageWidth,
//                        rendererPageHeight,
//                        Bitmap.Config.ARGB_8888);
//                rendererPage.render(bitmap, null, null,
//                        PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
//
//                imageView.setImageBitmap(bitmap);
//                rendererPage.close();
//
//                pdfRenderer.close();
//                fileDescriptor.close();
//                //                int IMG_WIDTH = imageView.getWidth();
////                int IMG_HEIGHT = imageView.getHeight();
////                Bitmap bitmap = Bitmap.createBitmap(IMG_WIDTH, IMG_HEIGHT, Bitmap.Config.ARGB_4444);
////                    //Log.d(TAG, "onActivityResult: file abs path: "+file.getAbsolutePath()+"file path:"+file.getPath());
////                    pdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));
//////            if(currentPage<0){
//////                currentPage=0;
//////            }
//////            else if(currentPage>pdfRenderer.getPageCount()) {
//////                currentPage=pdfRenderer.getPageCount()-1;
//////            }
////
////                Matrix m = imageView.getImageMatrix();
////                    Log.d(TAG, "onActivityResult: image height" + IMG_HEIGHT + " width" + IMG_WIDTH);
////                    Rect rect = new Rect(0, 0, IMG_WIDTH, IMG_HEIGHT);
////
////                    pdfRenderer.openPage(0).render(bitmap, rect, m, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
////                    imageView.setImageMatrix(m);
////                    imageView.setImageBitmap(bitmap);
////                    imageView.invalidate();
//
//                } catch(IOException e){
//                Log.d(TAG, "onActivityResult: CATCH block");
//                    e.printStackTrace();
//                }
//
//
//        }
//
//    }


  //  }
}
